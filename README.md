# ThinkLikeANurse

## Getting Started

To start the project run the following command: ` cd ThinkLikeANurse; npm i; npm run dev `

### Mocks

#### Home Page
![Home Page](/mocks/Home%20Page.png "Home Page")

#### Log In Page
![Home Page](/mocks/Login%20View.png "Log In Page")

#### Walkthrough
![Home Page](/mocks/Walkthrough.png "Walkthrough")

#### Guide Page
![Home Page](/mocks/Guide.png "Guide Page")

#### Teacher View
![Home Page](/mocks/Teacher%20View.png "Teacher View")

#### Write A Question View
![Home Page](/mocks/Teacher%20Write%20Question.png "Write A Question")

#### Review A Question View
![Home Page](/mocks/Question%20Review.png "Review A Question")

#### Student View
![Home Page](/mocks/Student%20View.png "Student View")