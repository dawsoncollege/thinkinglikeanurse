import { createTheme, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import { NavBar } from "./components/navbar/NavBar";
import { Footer } from "./components/footer/Footer";
import { Outlet } from "react-router-dom";
import { Box } from "@mui/material";
import "./App.css";
import "./assets/Inter.css";
import "./assets/Poppins.css";
declare module "@mui/material/styles" {
  interface Palette {
    tertiary: Palette["primary"];
  }

  interface PaletteOptions {
    tertiary: PaletteOptions["primary"];
  }
}

function App() {
  const theme = createTheme({
    typography: {
      fontFamily: "Inter, Arial",
      fontSize: 16,
      h1: {
        fontFamily: "Poppins, Arial",
        fontSize: 127.88,
      },
      h2: {
        fontFamily: "Poppins, Arial",
        fontSize: 90.44,
      },
      h3: {
        fontFamily: "Poppins, Arial",
        fontSize: 63.96,
      },
      h4: {
        fontFamily: "Poppins, Arial",
        fontSize: 45.23,
      },
      h5: {
        fontFamily: "Poppins, Arial",
        fontSize: 31.99,
      },
      h6: {
        fontFamily: "Poppins, Arial",
        fontSize: 22.62,
      },
    },
    palette: {
      primary: {
        main: "#3666ff",
      },
      secondary: {
        main: "#c8e3ff",
      },
      tertiary: {
        main: "#e6e6e6",
      },
      error: {
        main: "#f54949",
      },
      contrastThreshold: 4.5,
      text: {
        primary: "#011145",
      },
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
        }}
      >
        <NavBar />
        <Outlet />
        <Footer />
      </Box>
    </ThemeProvider>
  );
}

export default App;
