import {
  Box,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
  Stack,
  Toolbar,
} from "@mui/material";
import { Metacognition } from "../components/guide/metacognition";
import { QuestionTypes } from "../components/guide/questionTypes";
import { NursingProcess } from "../components/guide/nursingProcess";
import { Mosaic } from "../components/guide/mosaic";
import { StepByStep } from "../components/guide/stepByStep";
import { ExampleQuestion } from "../components/guide/exampleQuestion";
import { useState } from "react";

export const Guide = (): JSX.Element => {
  const headers = [
    {
      text: "Traditional Nursing Process",
      className: "nursingProcess",
    },
    {
      text: "Teaching and Learning",
      className: "metacognition",
    },
    { text: "Question Formats", className: "questionTypes" },
    { text: "Mosaic of Clinical Competencies", className: "mosaic" },
    {
      text: "Building and Understanding OIIQ-type Questions",
      className: "stepByStep",
    },
    { text: "Your Turn", className: "yourTurn" },
  ];

  const [activeId, setActiveId] = useState("");

  return (
    <Box sx={{ display: "flex" }} className="guide">
      <Drawer
        variant="permanent"
        sx={{
          width: 200,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: 200,
            boxSizing: "border-box",
          },
        }}
      >
        <Toolbar />
        <Box sx={{ overflow: "auto" }}>
          <List>
            {headers.map(({ text, className }, index) => (
              <ListItem key={text} disablePadding>
                <ListItemButton
                  component="a"
                  href={`#${className}`}
                  sx={{
                    textDecoration:
                      activeId === className ? "underline" : "none",
                  }}
                >
                  <ListItemText sx={{ textAlign: "center" }} primary={text} />
                </ListItemButton>
              </ListItem>
            ))}
          </List>
        </Box>
      </Drawer>
      <Box sx={{ flexGrow: 1 }}>
        <Stack
          divider={<Divider orientation="horizontal" sx={{ m: 8 }} flexItem />}
        >
          <NursingProcess callback={setActiveId} active={activeId} />
          <Metacognition callback={setActiveId} active={activeId} />
          <QuestionTypes callback={setActiveId} active={activeId} />
          <Mosaic callback={setActiveId} active={activeId} />
          <StepByStep callback={setActiveId} active={activeId} />
          <ExampleQuestion callback={setActiveId} active={activeId} />
        </Stack>
      </Box>
    </Box>
  );
};
