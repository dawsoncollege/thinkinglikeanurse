import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { Divider } from "@mui/material";

export const Home = (): JSX.Element => {
  return (
    <>
      <Box
        sx={{
          bgcolor: "background.paper",
          pt: 8,
          pb: 6,
        }}
      >
        <Container maxWidth="sm">
          <Typography
            component="h1"
            variant="h2"
            align="center"
            color="text.primary"
            gutterBottom
          >
            Welcome to Thinking Like a Nurse!
          </Typography>

          <Divider />
          <Typography
            variant="h6"
            align="center"
            color="text.secondary"
            paragraph
          >
            Your quick and easy guide to understanding and writing OIIQ
            questions. This platform is meant to provide you with the
            information necessary to understand an OIIQ question inside and out.
          </Typography>
          <Divider />
          <Typography
            variant="h6"
            align="center"
            color="text.secondary"
            paragraph
          >
            The OIIQ certification exam assesses the candidates' integration of
            the knowledge, skills, attitudes, and judgement required to make
            clinical decisions and intervene appropriately.
          </Typography>
          <Divider />
          <Typography
            variant="h6"
            align="center"
            color="text.secondary"
            paragraph
          >
            Successful completion of the OIIQ exam is required to practice
            nursing in Quebec.
          </Typography>
        </Container>
      </Box>
    </>
  );
};
