import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import { ErrorPage } from "./pages/Error";
import { Guide } from "./pages/Guide";
import { Home } from "./pages/Home";

window.global ||= window;
const router = createBrowserRouter([
  {
    path: "/thinkinglikeanurse/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "/thinkinglikeanurse/",
        element: <Home />,
      },
      {
        path: "guide/",
        element: <Guide />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
