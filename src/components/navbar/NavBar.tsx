import { Box, Button, Container } from "@mui/material";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import { forwardRef } from "react";
import {
  Link as RouterLink,
  LinkProps as RouterLinkProps,
} from "react-router-dom";

const LinkBehavior = forwardRef<any, Omit<RouterLinkProps, "to">>(
  (props, ref) => <RouterLink ref={ref} to="/" {...props} role={undefined} />
);

export const NavBar = (): JSX.Element => {
  return (
    <AppBar sx={{ zIndex: 1400 }} position="sticky" className="nav">
      <Container maxWidth="xl">
        <Toolbar sx={{ display: "flex", justifyContent: "left", gap: "400px" }}>
          <Box>
            <Button
              className="guideBtn"
              component={RouterLink}
              to="guide/"
              key={"guide"}
              sx={{ my: 2, color: "white", display: "block" }}
            >
              OIIQ Guide
            </Button>
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
};
