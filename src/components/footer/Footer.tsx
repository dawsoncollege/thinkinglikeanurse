import { Box, Container, Link, Typography } from "@mui/material";

export const Footer = (): JSX.Element => {
  return (
    <Box
      component="footer"
      sx={{
        zIndex: 1400,
        py: 3,
        px: 2,
        mt: "auto",
        backgroundColor: (theme) =>
          theme.palette.mode === "light"
            ? theme.palette.grey[200]
            : theme.palette.grey[800],
      }}
      className="footer"
    >
      <Container maxWidth="sm">
        <Typography variant="body2" color="text.secondary">
          {"Copyright © "}
          <Link
            color="inherit"
            href="https://dawsoncollege.gitlab.io/thinkinglikeanurse/"
          >
            Thinking Like a Nurse
          </Link>{" "}
          {new Date().getFullYear()}
          {"."}
        </Typography>
      </Container>
    </Box>
  );
};
