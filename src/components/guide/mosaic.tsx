import {
  Card,
  CardMedia,
  Container,
  Divider,
  List,
  ListItem,
  ListItemText,
  Tooltip,
  Typography,
} from "@mui/material";
import { StatusProps } from "./statusInterface";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";
import imgURL from "../../assets/Mosaic-OIIQ.jpg";
import mosaicData from "../../data/mosaic.json";

export const Mosaic = ({ callback, active }: StatusProps): JSX.Element => {
  const { ref, inView, entry } = useInView({ threshold: 0.1 });

  useEffect(() => {
    if (active !== "mosaic" && inView) {
      callback("mosaic");
    }
  });

  return (
    <Container id="mosaic" maxWidth="xl" ref={ref}>
      <Container
        maxWidth="xl"
        sx={{ bgcolor: "#3666ff", color: "#fff", p: 4, borderRadius: 4 }}
      >
        <Typography align="left" variant="h1" sx={{ m: 2 }}>
          {mosaicData.title}
        </Typography>
        <Typography align="left" variant="body1">
          {mosaicData.note}
        </Typography>
      </Container>

      <Container maxWidth="xl">
        <Card sx={{ maxWidth: 1100, m: 2 }}>
          <CardMedia
            component="img"
            alt="Mosaic of Nurse's clinical competencies"
            height="auto"
            image={imgURL}
          />
        </Card>
        {mosaicData.blurb.map((blurb, index) => (
          <Typography align="left" variant="body1" key={index}>
            {blurb}
          </Typography>
        ))}
      </Container>
      <Divider
        orientation="horizontal"
        variant="inset"
        sx={{ m: 5 }}
        flexItem
      />

      <Container maxWidth="xl">
        <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
          {mosaicData.functional.title}
        </Typography>
        {mosaicData.functional.subSection.map(
          ({ title, example, content }, index) => {
            return (
              <div key={index}>
                <Tooltip
                  title={<Typography fontSize={16}>{example}</Typography>}
                  placement="top-start"
                >
                  <Typography
                    align="left"
                    variant="h5"
                    color="primary"
                    sx={{ mt: 2, mb: 2, textDecoration: "underline" }}
                  >
                    {title}
                  </Typography>
                </Tooltip>

                {content.map((text, index) => (
                  <Typography align="left" variant="body1" key={index}>
                    {text}
                  </Typography>
                ))}
              </div>
            );
          }
        )}
      </Container>
      <Divider
        orientation="horizontal"
        variant="inset"
        sx={{ m: 5 }}
        flexItem
      />
      <Container maxWidth="xl">
        <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
          {mosaicData.professional.title}
        </Typography>
        {mosaicData.professional.subSection.map(
          ({ title, subSection }, index) => {
            return (
              <div key={index}>
                <Typography align="left" variant="h4" sx={{ mt: 2, mb: 2 }}>
                  {title}
                </Typography>
                {subSection.map(({ title, content, examples }, index) => {
                  return (
                    <div key={index}>
                      <Tooltip
                        sx={{ fontSize: 5 }}
                        title={
                          <List sx={{ m: 2 }}>
                            {examples.map((example, index) => {
                              return (
                                <ListItem
                                  sx={{
                                    display: "list-item",
                                    listStyle: "outside",
                                  }}
                                  disablePadding
                                  key={index}
                                >
                                  <Typography fontSize={16}>
                                    {example}
                                  </Typography>
                                </ListItem>
                              );
                            })}
                          </List>
                        }
                        placement="top-start"
                      >
                        <Typography
                          align="left"
                          variant="h6"
                          color="primary"
                          sx={{ mt: 2, mb: 2, textDecoration: "underline" }}
                        >
                          {title}
                        </Typography>
                      </Tooltip>

                      {content.map((text, index) => (
                        <Typography
                          align="left"
                          variant="body1"
                          sx={{ m: 2 }}
                          key={index}
                        >
                          {text}
                        </Typography>
                      ))}
                    </div>
                  );
                })}
                {title === "Clinical Assessment" ? (
                  <Typography
                    align="left"
                    variant="body2"
                    sx={{ mt: 2, mb: 2 }}
                  >
                    Note: Data collection is not enough, the student must be
                    able to collect appropriate and related data in order to
                    determine a CLINICAL JUDGEMENT.
                  </Typography>
                ) : null}
              </div>
            );
          }
        )}
      </Container>
    </Container>
  );
};
