import { Container, Divider, Stack, Typography } from "@mui/material";
import { useInView } from "react-intersection-observer";
import { StatusProps } from "./statusInterface";
import { useEffect } from "react";
import { ExamplePopup } from "./examplePopup";
import metacognitionData from "../../data/metacognition.json";

export const Metacognition = ({
  callback,
  active,
}: StatusProps): JSX.Element => {
  const { ref, inView, entry } = useInView({ threshold: 0.25 });

  useEffect(() => {
    if (active !== "metacognition" && inView) {
      callback("metacognition");
    }
  });

  return (
    <Container id="metacognition" maxWidth="xl" ref={ref}>
      <Container
        maxWidth="xl"
        sx={{ bgcolor: "#3666ff", color: "#fff", p: 4, borderRadius: 4 }}
      >
        <Typography align="left" variant="h1" sx={{ mt: 2, mb: 2 }}>
          {metacognitionData.title}
        </Typography>
        <Typography align="left" variant="body1" sx={{ m: 2 }}>
          {metacognitionData.blurb}
        </Typography>
      </Container>
      <Stack
        divider={
          <Divider
            orientation="horizontal"
            variant="inset"
            sx={{ m: 5 }}
            flexItem
          />
        }
      >
        {metacognitionData.content.map(
          ({ contentTitle, contentBlurb, contentExample }, index) => {
            return (
              <Container key={index} maxWidth="xl">
                <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
                  {contentTitle}
                </Typography>
                {contentBlurb.map((blurb, index) => {
                  return (
                    <Typography align="left" variant="body1" key={index}>
                      {blurb}
                    </Typography>
                  );
                })}
                <ExamplePopup popupText={contentExample} popupWidth={650} />
              </Container>
            );
          }
        )}
      </Stack>
    </Container>
  );
};
