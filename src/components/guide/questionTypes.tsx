import {
  Box,
  Container,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemText,
  Stack,
  Tooltip,
  Typography,
} from "@mui/material";
import { StatusProps } from "./statusInterface";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";
import questionTypeData from "../../data/questionTypes.json";
import { Question } from "./question/question";

export const QuestionTypes = ({
  callback,
  active,
}: StatusProps): JSX.Element => {
  const { ref, inView, entry } = useInView({ threshold: 0.1 });

  useEffect(() => {
    if (active !== "questionTypes" && inView) {
      callback("questionTypes");
    }
  });

  return (
    <Container id="questionTypes" maxWidth="xl" ref={ref}>
      <Container
        maxWidth="xl"
        sx={{ bgcolor: "#3666ff", color: "#fff", p: 4, borderRadius: 4 }}
      >
        <Typography align="left" variant="h2" sx={{ mt: 2, mb: 2 }}>
          {questionTypeData.title}
        </Typography>
        <Typography align="left" variant="body1" sx={{ m: 2 }}>
          {questionTypeData.blurb}
        </Typography>
      </Container>
      <Stack
        divider={
          <Divider
            orientation="horizontal"
            variant="inset"
            sx={{ m: 5 }}
            flexItem
          />
        }
      >
        {questionTypeData.questionTypes.map(
          ({ qType, typeInfo, questions, finalNote }, index) => {
            return (
              <Container maxWidth="xl" key={index}>
                <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
                  {qType}
                </Typography>
                {typeInfo.map((info, index) => (
                  <Typography align="left" variant="body1" key={index}>
                    {info}
                  </Typography>
                ))}
                {questions.map(
                  ({ questionId, context, question, options }, index) => {
                    return (
                      <Question
                        key={index}
                        questionId={questionId}
                        context={context}
                        question={question}
                        options={options}
                      />
                    );
                  }
                )}
                <Typography
                  align="left"
                  variant="body1"
                  key={index}
                  sx={{ mt: 2, mb: 2 }}
                >
                  {finalNote}
                </Typography>
              </Container>
            );
          }
        )}
        <Container maxWidth="xl">
          <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
            {questionTypeData.questionComponentsTitle}
          </Typography>
          <Typography align="left" variant="body1" sx={{ m: 2 }}>
            {questionTypeData.questionComponentsBlurb}
          </Typography>
          <Container maxWidth="xl">
            <List sx={{ m: 2 }}>
              {questionTypeData.questionComponentsListOptions.map(
                (listOption, index) => {
                  return (
                    <ListItem
                      key={index}
                      sx={{ display: "list-item", listStyle: "outside" }}
                      disablePadding
                    >
                      <ListItemText primary={listOption} />
                    </ListItem>
                  );
                }
              )}
            </List>
          </Container>
          {questionTypeData.questionComponents.map(
            ({ componentName, listItems }, index) => {
              return (
                <Container maxWidth="xl" key={index}>
                  <Typography
                    align="left"
                    variant="h4"
                    sx={{ mt: 2, mb: 2 }}
                    color="primary"
                  >
                    {componentName}
                  </Typography>
                  <List sx={{ m: 2 }}>
                    {listItems.map((item, index) => {
                      return (
                        <ListItem
                          sx={{ display: "list-item", listStyle: "outside" }}
                          disablePadding
                          key={index}
                        >
                          <ListItemText primary={item} />
                        </ListItem>
                      );
                    })}
                  </List>
                </Container>
              );
            }
          )}
        </Container>
      </Stack>
    </Container>
  );
};
