import {
  Container,
  List,
  ListItem,
  ListItemText,
  Tooltip,
  Typography,
} from "@mui/material";
import { StatusProps } from "./statusInterface";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";
import nursingProcess from "../../data/nursingProcess.json";

import parse from 'html-react-parser';

export const NursingProcess = ({
  callback,
  active,
}: StatusProps): JSX.Element => {
  const { ref, inView, entry } = useInView({ threshold: 0.25 });

  useEffect(() => {
    if (active !== "nursingProcess" && inView) {
      callback("nursingProcess");
    }
  });

  return (
    <Container id="nursingProcess" maxWidth="xl" ref={ref} sx={{ mt: 4 }}>
      <Container
        maxWidth="xl"
        sx={{ bgcolor: "#3666ff", color: "#fff", p: 4, borderRadius: 4 }}
      >
        <Typography align="left" variant="h1" sx={{ mt: 2, mb: 2 }}>
          {nursingProcess.title}
        </Typography>
        <Typography align="left" variant="body1" sx={{ maxWidth: 800 }}>
          {nursingProcess.blurb}
        </Typography>
        <List sx={{ m: 2 }}>
          {nursingProcess.processSteps.map((step, index) => {
            if (step === "Diagnosing") {
              return (
                <ListItem
                  sx={{ display: "list-item", listStyle: "outside" }}
                  disablePadding
                  key={index}
                >
                  <Tooltip
                    title={nursingProcess.stepNote}
                    placement="top-start"
                  >
                    <ListItemText
                      primary={step}
                      sx={{ textDecoration: "underline" }}
                    />
                  </Tooltip>
                </ListItem>
              );
            }
            return (
              <ListItem
                sx={{ display: "list-item", listStyle: "outside" }}
                disablePadding
                key={index}
              >
                <ListItemText primary={step} />
              </ListItem>
            );
          })}
        </List>
      </Container>
      <Container maxWidth="xl">
        {nursingProcess.nursingProcess.map(({ title, blurb }, index) => {
          return (
            <div key={title}>
              <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
                {title}
              </Typography>
              <Typography
                align="left"
                variant="body1"
                key={index}
                sx={{ maxWidth: 800 }}
              >
                {blurb}
              </Typography>
            </div>
          );
        })}
      </Container>
    </Container>
  );
};
