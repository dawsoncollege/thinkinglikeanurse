import { useState } from "react";
import { Box, Button, Popover, Typography } from "@mui/material";

interface PopupProps {
  popupText: string;
  popupWidth: number;
}

export const ExamplePopup = ({
  popupText,
  popupWidth,
}: PopupProps): JSX.Element => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <>
      <Box textAlign="center" sx={{ mt: 2 }}>
        <Button aria-describedby={id} variant="contained" onClick={handleClick}>
          Show me an example!
        </Button>
      </Box>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Typography sx={{ p: 2, width: popupWidth }}>{popupText}</Typography>
      </Popover>
    </>
  );
};
