import { Grid, Typography } from "@mui/material";
import "./Handwritten.css";
import patientInfo from "../../../data/joeyProblem.json";

export const PatientChart = (): JSX.Element => {
  return (
    <Grid
      container
      columns={4}
      sx={{
        "--Grid-borderWidth": "1px",
        borderTop: "var(--Grid-borderWidth) solid",
        borderLeft: "var(--Grid-borderWidth) solid",
        borderColor: "black",
        "& > div": {
          borderRight: "var(--Grid-borderWidth) solid",
          borderBottom: "var(--Grid-borderWidth) solid",
          borderColor: "black",
        },
        mt: 4,
      }}
    >
      <Grid item xs={3}>
        <Typography variant="h5" sx={{ paddingLeft: 1 }}>
          Thinking Like a Nurse
        </Typography>
      </Grid>
      <Grid item xs={1}>
        <Typography variant="h5" sx={{ paddingLeft: 1 }}>
          Patient Chart
        </Typography>
      </Grid>
      <Grid item xs={2}>
        <Typography sx={{ paddingLeft: 1 }}>Name :</Typography>
        <Typography className="handwritten">{patientInfo.name}</Typography>
      </Grid>
      <Grid item xs={1}>
        <Typography sx={{ paddingLeft: 1 }}>Age :</Typography>
        <Typography className="handwritten">{patientInfo.age}</Typography>
      </Grid>
      <Grid item xs={1}>
        <Typography sx={{ paddingLeft: 1 }}>Sex :</Typography>
        <Typography className="handwritten">{patientInfo.sex}</Typography>
      </Grid>
      <Grid item xs={4}>
        <Typography sx={{ paddingLeft: 1 }}>
          Personal Characteristics :
        </Typography>
        {patientInfo.personalCharacteristics.map((characteristic, index) => {
          return (
            <Typography className="handwritten" key={index}>
              {characteristic}
            </Typography>
          );
        })}
      </Grid>
      <Grid item xs={4}>
        <Typography sx={{ paddingLeft: 1 }}>Environment :</Typography>
        <Typography className="handwritten">
          {patientInfo.environment}
        </Typography>
      </Grid>
      <Grid item xs={4}>
        <Typography sx={{ paddingLeft: 1 }}>Medical History :</Typography>
        <Typography className="handwritten">
          {patientInfo.medicalHistory}
        </Typography>
      </Grid>
      <Grid item xs={4}>
        <Typography sx={{ paddingLeft: 1 }}>
          Clinical Manifestations :
        </Typography>
        <Typography className="handwritten">
          {patientInfo.clinicalManifestations}
        </Typography>
      </Grid>
    </Grid>
  );
};
