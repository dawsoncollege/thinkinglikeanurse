import { Box, Container, IconButton, Tooltip, Typography } from "@mui/material";
import { PatientChart } from "./chart/patientChart";
import { Question } from "./question/question";
import { StatusProps } from "./statusInterface";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";
import { Info } from "@mui/icons-material";
import questions from "../../data/yourTurnQuestions.json";

export const ExampleQuestion = ({
  callback,
  active,
}: StatusProps): JSX.Element => {
  const { ref, inView, entry } = useInView({ threshold: 0.3 });

  useEffect(() => {
    if (active !== "yourTurn" && inView) {
      callback("yourTurn");
    }
  });

  return (
    <Container id="yourTurn" maxWidth="xl" ref={ref}>
      <Container
        maxWidth="xl"
        sx={{ bgcolor: "#3666ff", color: "#fff", p: 4, borderRadius: 4 }}
      >
        <Typography align="left" variant="h1" sx={{ m: 2 }}>
          Your Turn
        </Typography>
      </Container>
      <Container maxWidth="xl">
        <PatientChart />
        {questions.map(({ questionId, context, question, options }, index) => (
          <Question
            key={index}
            questionId={questionId}
            context={context}
            question={question}
            options={options}
          />
        ))}
      </Container>
    </Container>
  );
};
