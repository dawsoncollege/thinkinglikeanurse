import {
  Container,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Radio,
  RadioGroup,
  Typography,
} from "@mui/material";
import { useState } from "react";

interface QuestionProps {
  questionId: string;
  context: string;
  question: string;
  options: Option[];
}

interface Option {
  optionText: string;
  optionDetails: string;
  correct: boolean;
}

export const Question = ({
  questionId,
  context,
  question,
  options,
}: QuestionProps): JSX.Element => {
  const [helperText, setHelperText] = useState("");

  return (
    <>
      <Typography align="left" variant="h4" sx={{ m: 2 }}>
        {questionId}
      </Typography>
      <Typography align="left" variant="body1" sx={{ m: 2 }}>
        {context}
      </Typography>
      <Container>
        <FormControl>
          <FormLabel id={`${questionId}`} sx={{ m: 1 }}>
            {question}
          </FormLabel>
          <RadioGroup
            aria-labelledby={`${questionId}`}
            name="radio-buttons-group"
          >
            {options.map(({ optionText, correct, optionDetails }, i) => {
              return (
                <FormControlLabel
                  key={i}
                  value={optionText}
                  control={
                    <Radio
                      color={correct ? "success" : "error"}
                      disableRipple={true}
                      onChange={(event) => {
                        setHelperText(optionDetails);
                      }}
                    />
                  }
                  label={optionText}
                />
              );
            })}
          </RadioGroup>
        </FormControl>
        <FormHelperText sx={{ fontSize: 15 }}>{helperText}</FormHelperText>
      </Container>
    </>
  );
};
