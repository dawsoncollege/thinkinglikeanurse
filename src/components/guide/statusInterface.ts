import { Dispatch, SetStateAction } from "react";

export interface StatusProps {
  callback: Dispatch<SetStateAction<string>>;
  active: string;
}
