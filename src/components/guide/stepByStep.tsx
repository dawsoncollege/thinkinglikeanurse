import {
  Container,
  List,
  ListItem,
  ListItemText,
  Typography,
} from "@mui/material";
import { StatusProps } from "./statusInterface";
import { useInView } from "react-intersection-observer";
import { useEffect } from "react";
import questionInfo from "../../data/buildingAQuestion.json";
import { PatientChart } from "./chart/patientChart";

export const StepByStep = ({ callback, active }: StatusProps): JSX.Element => {
  const { ref, inView, entry } = useInView({ threshold: 0.2 });
  useEffect(() => {
    if (active !== "stepByStep" && inView) {
      callback("stepByStep");
    }
  });

  return (
    <Container id="stepByStep" maxWidth="xl" ref={ref}>
      <Container
        maxWidth="xl"
        sx={{ bgcolor: "#3666ff", color: "#fff", p: 4, borderRadius: 4 }}
      >
        <Typography align="left" variant="h1" sx={{ m: 2 }}>
          Building and Understanding OIIQ-type Questions
        </Typography>
      </Container>
      <Container maxWidth="xl">
        <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
          {questionInfo.generalRules.title}
        </Typography>
        <List sx={{ m: 2 }}>
          {questionInfo.generalRules.rules.map((rule, index) => {
            return (
              <ListItem
                sx={{ display: "list-item", listStyle: "outside" }}
                disablePadding
                key={index}
              >
                <ListItemText primary={rule} />
              </ListItem>
            );
          })}
        </List>
      </Container>
      <Container maxWidth="xl">
        <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
          {questionInfo.contextualComponent.title}
        </Typography>
        {questionInfo.contextualComponent.subSections.map(
          ({ title, rules }, index) => {
            return (
              <Container maxWidth="xl" key={index}>
                <Typography
                  align="left"
                  variant="h4"
                  color="primary"
                  sx={{ mt: 2, mb: 2 }}
                >
                  {title}
                </Typography>
                <List sx={{ m: 2 }}>
                  {rules.map((rule, index) => {
                    return (
                      <ListItem
                        sx={{ display: "list-item", listStyle: "outside" }}
                        disablePadding
                        key={index}
                      >
                        <ListItemText primary={rule} />
                      </ListItem>
                    );
                  })}
                </List>
              </Container>
            );
          }
        )}
        <Typography align="left" variant="h4" sx={{ mt: 2, mb: 2 }}>
          Example
        </Typography>
        <PatientChart />
      </Container>
      <Container maxWidth="xl">
        <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
          {questionInfo.multipleChoiceQuestions.title}
        </Typography>
        <List sx={{ m: 2 }}>
          {questionInfo.multipleChoiceQuestions.rules.map((rule, index) => {
            return (
              <ListItem
                sx={{ display: "list-item", listStyle: "outside" }}
                disablePadding
                key={index}
              >
                <ListItemText primary={rule} />
              </ListItem>
            );
          })}
        </List>
      </Container>
      <Container maxWidth="xl">
        <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
          {questionInfo.distractors.title}
        </Typography>
        <List sx={{ m: 2 }}>
          {questionInfo.distractors.rules.map((rule, index) => {
            return (
              <ListItem
                sx={{ display: "list-item", listStyle: "outside" }}
                disablePadding
                key={index}
              >
                <ListItemText primary={rule} />
              </ListItem>
            );
          })}
        </List>
      </Container>
      <Container maxWidth="xl">
        <Typography align="left" variant="h3" sx={{ mt: 2, mb: 2 }}>
          {questionInfo.submitting.title}
        </Typography>
        <Typography align="left" variant="body1" sx={{ mt: 2, mb: 2 }}>
          {questionInfo.submitting.blurb}
        </Typography>
      </Container>
    </Container>
  );
};
